import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = () => {
  return (
    <nav className="col-md-2 d-none d-md-block bg-light sidebar">
      <div className="sidebar-sticky">
        <ul className="nav flex-column">
          <li className="nav-item">
            <Link to="/" className="nav-link">
              <i className="fas fa-home mr-2"></i>Reports
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/dashboard" className="nav-link">
              <i className="fas fa-chart-pie mr-2"></i>Dashboard
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Sidebar;