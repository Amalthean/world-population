import React from 'react';
import { Router, Route } from 'react-router-dom';
import history from '../history';

import Header from './Header';
import Sidebar from './Sidebar';
import Dashboard from './dashboard/Dashboard'
import Reports from './countries/Reports';
import CountryDetails from './countries/CountryDetails';

const App = () => {
  return (
    <Router history={history}>
      <Header />
      <div className="container-fluid main-content">
        <div className="row">
          <Sidebar />
          <Route path="/" exact component={Reports} />
          <Route path="/dashboard" exact component={Dashboard} />
          <Route path="/country/:name" exact component={CountryDetails} />
        </div>
      </div>
    </Router>
  );
};

export default App;