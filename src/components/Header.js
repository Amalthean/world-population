import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { filterBy } from '../actions';

class Header extends React.Component {

  handleChange = (e) => {
    this.props.filterBy(e.target.value);
  }

  render () {
    return (
      <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
        <Link to="/" className="navbar-brand col-sm-3 col-md-2 mr-0">
          <i className="fas fa-globe mr-2"></i>World Population
        </Link>
        <input
          type="text"
          placeholder="Search"
          aria-label="Search"
          className="form-control form-control-dark w-100"
          onChange={this.handleChange}
        />
        <ul className="navbar-nav px-3">
          <li className="nav-item text-nowrap">
            {/* <a className="nav-link" href="#">Sign out</a> */}
          </li>
        </ul>
      </nav>
    );
  }
};

export default connect(
  null,
  {
    filterBy,
  }
)(Header);