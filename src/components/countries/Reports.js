import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { fetchCountries, sortBy } from '../../actions';

import CountrySorter from './CountriesSorter';
import CountryList from './CountryList';

class Reports extends React.Component {

  componentDidMount() {
    if (this.props.countries.length === 0) {
      this.props.fetchCountries();
    }
  }

  renderCountryList = () => {
    if (this.props.countries && this.props.countries) {
      return <CountryList countries={this.props.countries} />;
    } else {
      return (
        <div className="spinner-grow" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }
  }

  render() {
    return (
      <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <div className="d-flex flex-column">
          <CountrySorter />
          <div className="p-5">
            {this.renderCountryList()}
          </div>
        </div>
      </main>

    );
  }
}


// TODO: externalize this mapping, reusable logic
const mapStateToProps = ({ countries, sorter, filter }) => {
  if (sorter.prop !== '') {
    countries = _.orderBy(countries, [country => country[sorter.prop]], [sorter.direction]);
  }
  if (filter.value !== '') {
    countries = countries.filter(country => {
      try {
        return String(country[filter.prop])
          .toLowerCase()
          .includes(
            String(filter.value).toLowerCase()
          );
      } catch (err) {
        // TODO: handle error properly
        console.log(err);
        return true;
      }
    });
  }
  return {
    countries,
  }
}

export default connect(
  mapStateToProps,
  {
    fetchCountries,
    sortBy,
  },
)(Reports);