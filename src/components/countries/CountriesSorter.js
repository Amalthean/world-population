import React from 'react';
import { connect } from 'react-redux';

import { sortBy } from '../../actions';

export class CountrySorter extends React.Component {

  sortBy = (propName) => () => {
    if (this.props.sorter.direction === 'asc') {
      this.props.sortBy(propName, 'desc');
    } else {
      this.props.sortBy(propName, 'asc');
    }
  }

  resetSorter = () => {
    this.props.sortBy();
  }

  render() {
    return (
      <div className="d-flex flex-column">
        <div className="btn-group mx-auto px-5" role="group" aria-label="Countr List Controls">
          <button type="button" onClick={this.sortBy('name')} className="btn btn-dark">
            Sort by Name
          </button>
          <button type="button" onClick={this.sortBy('population')} className="btn btn-dark">
            Sort by Population
          </button>
          <button type="button" onClick={this.resetSorter} className="btn btn-dark">
            <i className="far fa-window-close"></i>
          </button>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ sorter }) => ({ sorter });

export default connect(
  mapStateToProps,
  {
    sortBy,
  }
)(CountrySorter);
