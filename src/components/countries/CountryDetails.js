import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { formatNumber } from '../../utils';
import { fetchCountries } from '../../actions';

class CountryDetails extends React.Component {

  componentDidMount() {
    if (this.props.countries.length === 0) {
      this.props.fetchCountries();
    }
  }

  renderCountryDetails = () => {
    if(this.props.country) {
      return (
        <div className="jumbotron">
          <h3>{this.props.country.name} ({this.props.country.nativeName})</h3>
          <p>Population: <span>{formatNumber(this.props.country.population)}</span></p>
          <p>Borders: {this.props.country.borders.join(' ')}</p>
          <p>...</p>
          <Link to="/">Go Back</Link>
        </div>
      );
    }
  }

  render() {
    return (
      <main role="main" className="offset-md-2 col-md-10 px-4">{this.renderCountryDetails()}</main>
    );
  }
}

const mapStateToProps = ({ countries }, ownProps) => {
  const { name } = ownProps.match.params;
  return {
    countries,
    country: countries.length > 0 && countries.find(country => country.name === name),
  }
}


export default connect(
  mapStateToProps,
  {
    // maybe create a memoized selector
    fetchCountries,
  }
)(CountryDetails);