import React from 'react';
import { Link } from 'react-router-dom';

import { formatNumber } from '../../utils';

const CountryItem = (props) => {
  return (
    <li className="media my-2">
      <img src="http://via.placeholder.com/50" className="rounded-circle mr-3" alt="alt text" />
        <div className="media-body">
          <h5 className="mt-0 mb-1">{props.country.name}</h5>
          <p>Population: {formatNumber(props.country.population)}</p>
        <Link to={`/country/${props.country.name}`} className="float-right">See Details</Link>
      </div>
    </li>
  );
}

export default CountryItem;