import React from 'react';

import CountryItem from './CountryItem';

const CountryList = ({ countries }) => {

  const renderCountries = () => {
    return countries
    .map(country => <CountryItem key={country.name} country={country} />);
  }

  return <ul className="list-unstyled">{renderCountries()}</ul>;
}

export default CountryList;