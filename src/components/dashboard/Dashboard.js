import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getRandomColor } from '../../utils';
import { fetchCountries } from '../../actions';


import PieChart from 'react-minimal-pie-chart';


class Dashboard extends React.Component {

  componentDidMount() {
    if (this.props.countries.length === 0) {
      this.props.fetchCountries();
    }
  }

  render() {
    return (
      <main role="main" className="offset-md-2 col-md-10 px-4">
        <h3 className="text-center">Top 10 countries with the highest population</h3>
        <div className="pie-chart">
          <PieChart
            data={this.props.chartData}
            label
            labelStyle={{
              fontSize: '2px'
            }}
            radius={25}
            labelPosition={112}
          />
        </div>
      </main>
    );
  }
};


// TODO: externalize this mapping, reusable logic
const mapStateToProps = ({ countries }) => {
  let chartData = [];
  if (countries.length !== 0) {
    chartData = _.chain(countries)
      .map(c => {
        const { name, population, area } = c;
        return { 
          title: name,
          value: population, 
          density: (population % area),
          // TODO: add collor pallete, chart is always redrawn with random colors
          color: getRandomColor(),
        }
      })
      .orderBy([c => c.value], ['desc'])
      .take(10)
      .value();
  }

  return {
    countries,
    chartData,
  }
}

export default connect(
  mapStateToProps,
  {
    fetchCountries,
  }
)(Dashboard);

