import { combineReducers } from 'redux';
import {
  FETCH_COUNTRIES,
  SORT_COUNTRIES,
  FILTER_COUNTRIES,
} from '../actions/action-types';

const INITIAL_SORTER_STATE = {
  prop: '',
  direction: 'asc',
}

const INITIAL_FILTER_STATE = {
  prop: 'name',
  value: '',
}

function countriesReducer(state = [], action) {
  switch (action.type) {
    case FETCH_COUNTRIES:
      return  [...action.payload];
    case SORT_COUNTRIES:
      return [...state];
    case FILTER_COUNTRIES:
      return [...state];
    default:
      return state;
  }
}

function sortingReducer(state = INITIAL_SORTER_STATE, action) {
  switch (action.type) {
    case SORT_COUNTRIES:
      return {
        ...state,
        prop: action.payload.prop,
        direction: action.payload.direction
      }
    default:
      return state;
  }
}

function filterReducer (state = INITIAL_FILTER_STATE, action) {
  switch (action.type) {
    case FILTER_COUNTRIES:
      return {
        ...state,
        prop: action.payload.prop,
        value: action.payload.value,
      }
    default:
      return state;
  }
}

export default combineReducers({
  countries: countriesReducer,
  sorter: sortingReducer,
  filter: filterReducer,
})