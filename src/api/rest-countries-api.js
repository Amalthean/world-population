import axios from 'axios';

export default axios.create({
  baseURL: 'https://restcountries-v1.p.rapidapi.com',
  headers: {
    'X-RapidAPI-Key': '480d993b6dmsh14033ba522ce0f4p1f353bjsn45348786700e'
  },
})
