// Function that manually creates date with YYYY-MM-DD format
export const formatDate = (date) => {
  const year = date.getFullYear();
  const month = (date.getMonth() + 1) < 10 ? `0${(date.getMonth() + 1)}` : (date.getMonth() + 1);
  const day = date.getDate();
  return `${year}-${month}-${day}`;
}

// Formats number e.g., XXX.XXX.XXX
export const formatNumber = (num) => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

export const getRandomColor = () => {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
