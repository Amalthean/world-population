import restCountriesApi from '../api/rest-countries-api';
import {
  FETCH_COUNTRIES,
  SORT_COUNTRIES,
  FILTER_COUNTRIES,
} from './action-types';

// FETCH_COUNTRIES_INFO action creator with promise
// export const fetchCountries = () => (dispatch, getState) => restCountriesApi.get('/all')
//   .then(res => {
//     dispatch({
//       type: FETCH_COUNTRIES,
//       payload: res.data || [],
//     });
//   })
//   .catch(err => console.log(err));


// FETCH_COUNTRIES action creator with async await
export const fetchCountries = () => async dispatch => {
  const res = await restCountriesApi.get('/all');

  dispatch({ type: FETCH_COUNTRIES, payload: res.data});
};

// SORT_COUNTRIES
export const sortBy = (prop = '', direction = '') => {
  return {
    type: SORT_COUNTRIES,
    payload: {
      prop,
      direction,
    }
  };
};


// FILTER_COUNTRIES
export const filterBy = (value, prop = 'name') => {
  return {
    type: FILTER_COUNTRIES,
    payload: {
      prop,
      value,
    }
  };
};