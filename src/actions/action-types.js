// ​ https://rapidapi.com/fayder/api/rest-countries-v1​
export const FETCH_COUNTRIES = 'FETCH_COUNTRIES';

export const SORT_COUNTRIES = 'SORT_COUNTRIES';
export const FILTER_COUNTRIES = 'FILTER_COUNTRIES';